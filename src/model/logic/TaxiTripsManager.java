package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.*;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.LinkedListClass;

public class TaxiTripsManager implements ITaxiTripsManager {

	private LinkedList<Service> servicios;
	
	private LinkedList<Taxi> taxis;
	
	public TaxiTripsManager() {
		servicios = new LinkedListClass<Service>();
		taxis = new LinkedListClass<Taxi>();
	}
	
	public void loadServices (String serviceFile) {
		BufferedReader reader = null;
		try {			
			Gson gson = new GsonBuilder().create();
			Taxi[] taxi = gson.fromJson(new FileReader(serviceFile), Taxi[].class);
			Service[] ser = gson.fromJson(new FileReader(serviceFile), Service[].class);
			ArrayList<String> ids = new ArrayList<String>();
			int cl=0;
			for(int i=0; i < taxi.length; i++) {
				if(!ids.contains(taxi[i].getTaxiId())) {
					ids.add(taxi[i].getTaxiId());
					taxis.add(taxi[i]);
					cl++;
				}
				servicios.add(ser[i]);
			}				 			
			
			System.out.println(cl);	
			System.out.println("Inside loadServices with " + serviceFile);
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		finally {
			if( reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxisOfCompany with " + company);
		return null;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		return null;
	}


}
