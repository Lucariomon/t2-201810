package model.data_structures;

public class Nodo<T extends Comparable> {

	private Nodo next;
	
	private T item;
	
	public Nodo( T itemP) {
		item = itemP;
		next = null;
	}
	
	public Nodo next() {
		return next;
	}
	
	public void cambiarNext( Nodo nextP) {
		next = nextP;
	}
	
	public T darItem() {
		return item;
	}
	
	public T cambiarItem( T nItem) {
		T bItem = item;
		item = nItem;
		return bItem;
	}
}
