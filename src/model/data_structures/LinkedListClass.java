package model.data_structures;

public class LinkedListClass <T extends Comparable> implements LinkedList {
	
	private Nodo ultimo;
	
	private Nodo primero;
	
	private int size;
	
	public LinkedListClass() {
		ultimo = null;
		primero = null;
		size = 0;
	}
	
	@Override
	public void add(Comparable item) {
		Nodo naw = new Nodo(item);
		if(primero == null) {
			primero = naw;
			ultimo = naw;
		}					
		ultimo.cambiarNext(naw);
		ultimo = naw;
		size++;
	}

	@Override
	public void delete(Comparable item) {
		if(primero == null)
			return;
		Nodo act = primero;
		while(act.next() != null)
			if(act.next().darItem().equals(item)) {
				Nodo b = act.next();
				act.cambiarNext(act.next().next());
				b.cambiarNext(null);
				size--;
				return;
			}				
	}

	@Override
	public Comparable get(Comparable item) {
		Nodo act = primero;
		while(act != null)
			if (act.darItem().equals(item))
				return act.darItem();
			else
				act = act.next();
		return null;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Comparable get(int pos) {	
		int su=0;
		Nodo act = primero;
		while(act != null) {
			if(su==pos)
				return act.darItem();
			act = act.next();
		}
		return null;
	}

	@Override
	public Comparable[] listing() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Comparable getCurrent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Comparable next() {


		return null;
	}

	@Override
	public Nodo primero() {		
		return primero;
	}

	@Override
	public Nodo ultimo() {
		return ultimo;
	}

	
}
